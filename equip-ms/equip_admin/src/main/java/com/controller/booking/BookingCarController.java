package com.controller.booking;

import itri.group.param.Pa;
import itri.group.param.RsMsg;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dao.booking.BookingCarDao;
import com.dao.project.ProjectDao;
import com.google.gson.Gson;
import com.security.SecurityController;

/******************************************
 * PlaceController.java
 * PlaceDao.java
 * placeAssign.js
 * placeView.jsp
 *
 * @author  create by 2016/12/08
 *******************************************/

@Controller
@RequestMapping(value="booking")
public class BookingCarController extends SecurityController{
	
	@Autowired
	Gson gson;
	
	
	@Autowired
	BookingCarDao bookingCarDao;
	
	@RequestMapping()
	public String shipDocAssign(ModelMap model) {
	    return "booking/bookingCarView";
	}
	
		
	@RequestMapping(value="/create",method = RequestMethod.POST)
	public @ResponseBody Object create(
			@RequestParam(value="projId",defaultValue="") String projId,
			@RequestParam(value="bookingDate",defaultValue="") String bookingDate,
			@RequestParam(value="startTime",defaultValue="") String startTime,
			@RequestParam(value="endTime",defaultValue="") String endTime,
			@RequestParam(value="cost",defaultValue="") String cost,
			@RequestParam(value="destination",defaultValue="") String destination,
			ModelMap model) {
		try {
			bookingCarDao.create(bookingDate, startTime,endTime,"a12345",projId,cost,destination,getUpdateDate(),"a12345");
			return Pa.success;
		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e);
		}
	}		
}
