package com.dao.booking;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class BookingCarDao {
	@Autowired
	JdbcTemplate jdbcTemplate;

/*	public Object find(String projId,String page){
		StringBuilder sql= new StringBuilder("select * from booking_car_doc ");
		List query=new ArrayList();
		if(StringUtils.isNotBlank(projId)){
			sql.append("where projId like ? and ");
			query.add(projId);
		}
		if(query.size()>0){
			sql.delete(sql.length()-4,	sql.length());
		}
		if(StringUtils.isNotBlank(page)){
			sql.append(String.format("limit  %s,%d ",(Integer.parseInt(page)-1)*Pa.PAGE_NUM,Pa.PAGE_NUM));
		}else{
			sql.append(String.format("limit  %d ",Pa.PAGE_NUM));
		}
		
		System.out.println(sql);
		List list=jdbcTemplate.queryForList(sql.toString(),query.toArray());
		return list;
	}

	public Object findPage(String projId){

		StringBuilder sql= new StringBuilder("select count(*) as total from proj ");
		List query=new ArrayList();
		if(StringUtils.isNotBlank(projId)){
			sql.append("where projId like ? and ");
			query.add(projId);
		}
		if(query.size()>0){
			sql.delete(sql.length()-4,	sql.length());
		}
		System.out.println(sql);
		Map<String,Object> rs=jdbcTemplate.queryForList(sql.toString(),query.toArray()).get(0);
		rs.put("page_num", Pa.PAGE_NUM);
		return rs;
	} 	
	
	@Transactional
	public void update(String projId,String startDate,String endDate,String projBudget,String updateDate,String updator){
    	String condi="update proj set projId=?,startDate=?,endDate=?,projBudget=?,updateDate=?,updator=? where projId=?";
    	jdbcTemplate.update(condi,new Object[]{projId,startDate,endDate,projBudget,updateDate,updator,projId});

	}*/
	
	@Transactional
	public void create(String bookingDate,String startTime,String endTime,String userId,String projId,String cost,String destination,String updateDate,String updator){
		String condi="insert booking_car_doc (bookingDate,startTime,endTime,userId,projId,cost,destination,returnTime,endMilage,carLocation,updateDate,updator) values (?,?,?,?,?,?,?,?,?,?,?,?)";
		jdbcTemplate.update(condi,new Object[]{bookingDate,startTime,endTime,userId,projId,cost,destination,null,null,null,updateDate,updator});	
	}
}