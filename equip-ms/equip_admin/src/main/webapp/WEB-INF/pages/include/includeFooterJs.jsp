<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

		
		<!-- Vendor -->
		<script src="<c:out value="${applicationScope.lib_link}"/>/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
		<script src="<c:out value="${applicationScope.lib_link}"/>/vendor/bootstrap/js/bootstrap.js"></script>
		<script src="<c:out value="${applicationScope.lib_link}"/>/vendor/nanoscroller/nanoscroller.js"></script>
		<script src="<c:out value="${applicationScope.lib_link}"/>/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script src="<c:out value="${applicationScope.lib_link}"/>/vendor/magnific-popup/jquery.magnific-popup.js"></script>
		<script src="<c:out value="${applicationScope.lib_link}"/>/vendor/jquery-placeholder/jquery-placeholder.js"></script>
		<script src="<c:out value="${applicationScope.lib_link}"/>/vendor/sweetalert/sweetalert.min.js"></script>
		

		<script src="<c:out value="${applicationScope.lib_link}"/>/vendor/jquery-ui/jquery-ui.js"></script>
		<script src="<c:out value="${applicationScope.lib_link}"/>/vendor/jqueryui-touch-punch/jqueryui-touch-punch.js"></script>
		<script src="<c:out value="${applicationScope.lib_link}"/>/vendor/select2/js/select2.js"></script>
		<script src="<c:out value="${applicationScope.lib_link}"/>/vendor/bootstrap-multiselect/bootstrap-multiselect.js"></script>
		<script src="<c:out value="${applicationScope.lib_link}"/>/vendor/jquery-maskedinput/jquery.maskedinput.js"></script>
		<script src="<c:out value="${applicationScope.lib_link}"/>/vendor/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>
		<script src="<c:out value="${applicationScope.lib_link}"/>/vendor/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
		<script src="<c:out value="${applicationScope.lib_link}"/>/vendor/bootstrap-timepicker/bootstrap-timepicker.js"></script>
		<script src="<c:out value="${applicationScope.lib_link}"/>/vendor/fuelux/js/spinner.js"></script>
		<script src="<c:out value="${applicationScope.lib_link}"/>/vendor/dropzone/dropzone.js"></script>
		<script src="<c:out value="${applicationScope.lib_link}"/>/vendor/bootstrap-markdown/js/markdown.js"></script>
		<script src="<c:out value="${applicationScope.lib_link}"/>/vendor/bootstrap-markdown/js/to-markdown.js"></script>
		<script src="<c:out value="${applicationScope.lib_link}"/>/vendor/bootstrap-markdown/js/bootstrap-markdown.js"></script>
		<script src="<c:out value="${applicationScope.lib_link}"/>/vendor/codemirror/lib/codemirror.js"></script>
		<script src="<c:out value="${applicationScope.lib_link}"/>/vendor/codemirror/addon/selection/active-line.js"></script>
		<script src="<c:out value="${applicationScope.lib_link}"/>/vendor/codemirror/addon/edit/matchbrackets.js"></script>
		<script src="<c:out value="${applicationScope.lib_link}"/>/vendor/codemirror/mode/javascript/javascript.js"></script>
		<script src="<c:out value="${applicationScope.lib_link}"/>/vendor/codemirror/mode/xml/xml.js"></script>
		<script src="<c:out value="${applicationScope.lib_link}"/>/vendor/codemirror/mode/htmlmixed/htmlmixed.js"></script>
		<script src="<c:out value="${applicationScope.lib_link}"/>/vendor/codemirror/mode/css/css.js"></script>
		<script src="<c:out value="${applicationScope.lib_link}"/>/vendor/summernote/summernote.js"></script>
		<script src="<c:out value="${applicationScope.lib_link}"/>/vendor/bootstrap-maxlength/bootstrap-maxlength.js"></script>
		<script src="<c:out value="${applicationScope.lib_link}"/>/vendor/ios7-switch/ios7-switch.js"></script>
		<script src="<c:out value="${applicationScope.lib_link}"/>/vendor/bootstrap-confirmation/bootstrap-confirmation.js"></script>
				
		<script src="<c:out value="${applicationScope.lib_link}"/>/javascripts/ajaxOnlyPlugin.js"></script>
		<script src="<c:out value="${applicationScope.lib_link}"/>/javascripts/handlebars-v4.0.5.js"></script>
		<script src="<c:out value="${applicationScope.lib_link}"/>/javascripts/jquery.cookie.js"></script>
		<script src="<c:out value="${applicationScope.lib_link}"/>/javascripts/jquery.form.min.js"></script>
		
				
		<!-- Theme Base, Components and Settings -->
		<script src="<c:out value="${applicationScope.lib_link}"/>/javascripts/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="<c:out value="${applicationScope.lib_link}"/>/javascripts/theme.custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="<c:out value="${applicationScope.lib_link}"/>/javascripts/theme.init.js"></script>

<%-- This is JSP comment 
<script src="3pty/js/jquery.cookie.js"></script>
<script src="3pty/js/url.min.js"></script>
<script>
var host='<c:out value="${pageContext.request.contextPath}"/>';
$.cookie.json = true;
</script>
--%>
			