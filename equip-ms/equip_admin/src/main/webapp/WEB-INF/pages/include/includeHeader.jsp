<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	<head>
		<!-- Basic -->
		<meta charset="UTF-8">
		<title>ITRI IOT後端管理平台</title>
		<meta name="keywords" content="HTML5 Admin Template" />
		<meta name="description" content="Porto Admin - Responsive HTML5 Template">
		<meta name="author" content="okler.net">
		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<!-- Web Fonts  -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

		<link rel="shortcut icon" href="<c:out value="${applicationScope.lib_link}"/>/images/favicon.ico">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="<c:out value="${applicationScope.lib_link}"/>/vendor/bootstrap/css/bootstrap.css" />
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" />
		<link rel="stylesheet" href="<c:out value="${applicationScope.lib_link}"/>/vendor/magnific-popup/magnific-popup.css" />
		<link rel="stylesheet" href="<c:out value="${applicationScope.lib_link}"/>/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css" />
		<link rel="stylesheet" href="<c:out value="${applicationScope.lib_link}"/>/vendor/sweetalert/sweetalert.css">
		
		<link rel="stylesheet" href="<c:out value="${applicationScope.lib_link}"/>/vendor/jquery-ui/jquery-ui.css" />
		<link rel="stylesheet" href="<c:out value="${applicationScope.lib_link}"/>/vendor/jquery-ui/jquery-ui.theme.css" />
		<link rel="stylesheet" href="<c:out value="${applicationScope.lib_link}"/>/vendor/select2/css/select2.css" />
		<link rel="stylesheet" href="<c:out value="${applicationScope.lib_link}"/>/vendor/select2-bootstrap-theme/select2-bootstrap.min.css" />
		<link rel="stylesheet" href="<c:out value="${applicationScope.lib_link}"/>/vendor/bootstrap-multiselect/bootstrap-multiselect.css" />
		<link rel="stylesheet" href="<c:out value="${applicationScope.lib_link}"/>/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css" />
		<link rel="stylesheet" href="<c:out value="${applicationScope.lib_link}"/>/vendor/bootstrap-colorpicker/css/bootstrap-colorpicker.css" />
		<link rel="stylesheet" href="<c:out value="${applicationScope.lib_link}"/>/vendor/bootstrap-timepicker/css/bootstrap-timepicker.css" />
		<link rel="stylesheet" href="<c:out value="${applicationScope.lib_link}"/>/vendor/dropzone/basic.css" />
		<link rel="stylesheet" href="<c:out value="${applicationScope.lib_link}"/>/vendor/dropzone/dropzone.css" />
		<link rel="stylesheet" href="<c:out value="${applicationScope.lib_link}"/>/vendor/bootstrap-markdown/css/bootstrap-markdown.min.css" />
		<link rel="stylesheet" href="js/vendor/summernote/summernote.css"/>
		<link rel="stylesheet" href="<c:out value="${applicationScope.lib_link}"/>/vendor/codemirror/lib/codemirror.css" />
		<link rel="stylesheet" href="<c:out value="${applicationScope.lib_link}"/>/vendor/codemirror/theme/monokai.css" />

		<link rel="stylesheet" href="css/elusive-icons/css/elusive-icons.min.css" />
<!-- 		<link rel="stylesheet" href="http://code.rifix.net/elusiveicons/2.0.0/css/elusive-icons.min.css"> -->

		<!-- Theme CSS -->
		<link rel="stylesheet" href="<c:out value="${applicationScope.lib_link}"/>/stylesheets/theme.css" />

		<!-- Skin CSS -->
		<link rel="stylesheet" href="<c:out value="${applicationScope.lib_link}"/>/stylesheets/skins/default.css" />

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="<c:out value="${applicationScope.lib_link}"/>/stylesheets/theme-custom.css">

		<!-- Head Libs -->
		<script src="<c:out value="${applicationScope.lib_link}"/>/vendor/modernizr/modernizr.js"></script>
		<script src="<c:out value="${applicationScope.lib_link}"/>/vendor/jquery/jquery.js"></script>
		<script src="<c:out value="${applicationScope.lib_link}"/>/javascripts/url.min.js"></script>

	</head>
