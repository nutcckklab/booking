<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!doctype html>
 <!-- sidebar-light -->
<html class="fixed sidebar-light" lang="zh-Hant-TW">
<%@include file="../include/includeHeader.jsp"%>
	<body>
		<section class="body">

			<!-- start: header -->
<%@include file="../include/includeTopHeader.jsp"%>
			<!-- end: header -->

			<div class="inner-wrapper">
				<!-- start: sidebar -->
<%@include file="../include/includeMenu.jsp"%>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<header class="page-header">
						<h2>預約</h2>
					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="index.html">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li><span>預約</span></li>
								<li><span>車輛預約</span></li>
							</ol>
							&nbsp;&nbsp;&nbsp;
						</div>
					</header>

						
					<!-- 新增 area -->
					<section class="panel panel-featured-primary panel-featured" role="create"  >
						<header class="panel-heading">
							<h3 class="panel-title"><i class="el el-file-new">車輛預約</i></h3>
						</header>						
						<div class="panel-body">
							<form class="form-horizontal form-bordered" id="createForm">
								<div class="form-group">
									<label class="col-md-2 control-label">計畫代號</label>
									<div class="col-md-6">
							            <select name="projId" id="projId"></select>
										<!--<input name=projId class="form-control" type="text">-->
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label">預約日期</label>
									<div class="col-md-6">
										<input name="bookingDate" class="form-control" type="text">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label">開始時間</label>
									<div class="col-md-6">
										<select name="startTime" id="startTime">
											<option value=0>0</option>
											<option value=1>1</option>
											<option value=2>2</option>
											<option value=3>3</option>
											<option value=4>4</option>
											<option value=5>5</option>
											<option value=6>6</option>
											<option value=7>7</option>
											<option value=8>8</option>
											<option value=9>9</option>
											<option value=10>10</option>
											<option value=11>11</option>
											<option value=12>12</option>
											<option value=13>13</option>
											<option value=14>14</option>
											<option value=15>15</option>
											<option value=16>16</option>
											<option value=17>17</option>
											<option value=18>18</option>
											<option value=19>19</option>
											<option value=20>20</option>
											<option value=21>21</option>
											<option value=22>22</option>
											<option value=23>23</option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label">結束時間</label>
									<div class="col-md-6">
										<select name="endTime" id="endTime"></select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label">費用</label>
									<div class="col-md-6">
										<input name="cost" class="form-control" type="text">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label">目的地</label>
									<div class="col-md-6">
										<input name="destination" class="form-control" type="text">
									</div>
								</div>
								
								<div class="text-center">
									<button class="btn btn-default btn-lg" onclick="ui.gotoMode('MODE_CREATE_CANCEL');"><i class="el el-return-key"></i>取消退出</button>
									<button class="btn btn-primary btn-lg" onclick="ui.showConfirmDialog('CONFIRM_CREATE');"><i class="el el-ok"></i>確定預約</button>										
								</div>						
							</form>
						</div>
					</section>
					<!-- end 新增area -->
						
			</div>

		</section>

<%@include file="../include/includeFooterJs.jsp"%>
		<script src="js/booking/bookingCar.js?<c:out value="${applicationScope.js_version}"/>"></script>
	</body>
</html>