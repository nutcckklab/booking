
--------餐廳select combobox------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
$("#queryForm select[name='orgRestId'],#queryForm select[name='targetRestId'],#createForm select[name='targetRestId']")	
.select2({
    placeholder : "要尋找的餐廳名稱",
    minimumInputLength: 1,
    allowClear: true,
    ajax:{
    	url: function (params) {
    	      return './combobox/findRest';
    	 },dataType: 'json',
    	 processResults: function (data, params) {
    		 console.debug(data);
    		 return {results: data};
	    }	
    }
});
	
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">出貨餐廳</label><br/>
												<select name="orgRestId" class="form-control"></select>
											</div>
										</div>





--------日期 datepicker------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
$('#dp1').datepicker("setDate", new Date());
	$('#dp1').datepicker({
		format: "yyyy/mm/dd",
		autoclose: true,
		orientation: "bottom auto"
	});

										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">進貨日期(yyyymmdd)</label>
												<input name="receiveDate" class="form-control" type="text">
											</div>
										</div>






											
--------日期 datepicker------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		  var subDay=subDate($('#queryForm input[name="sd"]').val(),
					$('#queryForm input[name="ed"]').val());

function getDateFromStr(str){
	if(str==null)return null;
	str=str.replace(/\//g,"");
	if(str.length!=8){
		return null;
	}else{
		return new Date(str.substr(0,4),
				str.substr(4,2),
				str.substr(6,2),
				0,0,0,0);
	}
}

function subDate(d1,d2){
	d1=getDateFromStr(d1);
	d2=getDateFromStr(d2);
	var d3=(d2-d1)/86400000;
	return d3;
}			



--------spring insert  get key------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
				KeyHolder holder = new GeneratedKeyHolder();
				 //為了要取insert auto gen key所以用PreparedStatementCreator
				final PreparedStatementCreator psc = new PreparedStatementCreator() {
				      @Override
				      public PreparedStatement createPreparedStatement(final Connection connection) throws SQLException {
				        final PreparedStatement ps = connection.prepareStatement("insert into mat_lot_stock (traceCode,matId,qty,unit,inboundDocId,inboundRestId,inboundDate,updateDate,updator) "
								+ "values (?,?,?,?,?,?,?,?,?)",
				            Statement.RETURN_GENERATED_KEYS);
				        ps.setString(1, m.get("traceCode").toString());
				        ps.setString(2, m.get("matId").toString());
				        ps.setString(3, m.get("qty").toString());
				        ps.setString(4, m.get("unit").toString());
				        ps.setString(5, inboundDocId);
				        ps.setString(6, targetRestId);
				        ps.setString(7, outDate);
				        ps.setString(8, updateDate);
				        ps.setString(9, updator);
				        return ps;
				      }
				};
			    jdbcTemplate.update(psc, holder);
			    final long matLotStockId = holder.getKey().longValue();				
				   
			    //只是方便debug
				sql=String.format("insert into mat_lot_stock (traceCode,matId,qty,unit,inboundDocId,inboundRestId,inboundDate,updateDate,updator) "
						+ "values ('%s',%s,%s,'%s','%s','%s','%s','%s','%s')", 
						m.get("traceCode").toString(),
						m.get("matId").toString(),
						m.get("qty").toString(),
						m.get("unit").toString(),
						inboundDocId,
						targetRestId,
						outDate,
						updateDate,
						updator);
				System.out.println(sql);
				//--------------------------------------------------------------------------------------	

//-------------------------------------------------------------------------------------------------------------dialog
				$.magnificPopup.close();
				
					$.magnificPopup.open({
						items:{
							src:'#uploadDialog'
						},
						preloader: false,
						modal: true
					});
					
					
		<div id="uploadDialog" class="modal-block modal-block-primary mfp-hide">
			<section class="panel">
				<div class="panel-body">
					<h4><strong style="color: red;">step1:</strong>選擇分貨食材</h4>
				</div>
				<footer class="panel-footer">
					<div class="row">
						<div class="col-md-12 text-right">
							<button class="btn btn-default modal-dismiss"  onclick="ui.createBatchDetailDialog('close');">取消</button>
							<button class="btn btn-primary modal-confirm" onclick="ui.createBatchDetailDialog('create');">新增</button>
						</div>
					</div>
				</footer>
			</section>
		</div>

				
//-------------------------------------------------------------------------------------------------------------資料上傳
JSP
<form id="uploadForm">
    <input type="file" id="btnFileUpload" accept="image/*" capture="camera" name="file">
</form>
javascript
    $("#btnFileUpload:file").change(function (){
           var fileName = $(this).val();
           console.debug(fileName);
           if(fileName.indexOf('.jpg')!=-1){
               module.upload.createData();
           }else{
               alert('抱歉我們只支援JPEG格式照片');
           }
    });
   
upload : {
        getParam : function() {
            console.debug($("#uploadForm input").get());
            var oMyForm = new FormData(document.getElementById("uploadForm"));
            oMyForm.append("file", $("#uploadForm input").get()[0]);
//            oMyForm.append("file", document.getElementById("uploadForm").file[0]);
            oMyForm.append("disName",$('#inputCustomerPhone').val()+'_'+new Date().valueOf());
            return oMyForm;
        },
        createData : function() {
            if($('#inputCustomerPhone').val().length==0){
                alert('請先輸入聯絡手機才能上傳照片');
                return;
            }
            $.ajax({
                url : './clientCustomerReqiureForm/uploadFile',
                data : module.upload.getParam(),
              dataType: 'json',
              processData: false,
              contentType: false,
              type: 'POST',
              success: function(data){
                  console.debug(data);
                    var source=$('#photo-template').html();
                    var template= Handlebars.compile(source);
                    var html= template(data);
                  $('#imgDisplay').append(html);
                  $('#btnFileUpload').val('');
                  
//                  initPhotoSwipeFromDOM('#imgDisplay');
              }
            });
        }
    },



controller
    //上傳客製修繕描述照片
    @RequestMapping(value = "/uploadFile", method = RequestMethod.POST)
    public @ResponseBody Object uploadFile(
            @RequestParam("file") MultipartFile file,
            @RequestParam(value="disName",defaultValue="") String disName,
            ModelMap model) throws UnsupportedEncodingException {
        if (!file.isEmpty()) {
            try {
                File dir=new File(Pa.CUSTOMER_ORDER_PATH +"temp");
                if(!dir.exists())
                    dir.mkdir();
                System.out.println(disName);
                BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(Pa.CUSTOMER_ORDER_PATH +"temp"+File.separator+ disName+".jpg")));
                FileCopyUtils.copy(file.getInputStream(), stream);
                
                stream.flush();
                stream.close();
            }
            catch (Exception e) {
                e.printStackTrace();
            }            
        }
        return  gson.toJson(new RsMsg(disName,disName));
    }
    
    //下載客製修繕照片
    @RequestMapping(value = "/img/{imageId}", method = RequestMethod.GET)
    public  ResponseEntity<byte[]> getFile(
            @PathVariable String imageId
            ) throws FileNotFoundException, IOException {
        File dir=new File(Pa.CUSTOMER_ORDER_PATH +"temp");
        if(!dir.exists())
            dir.mkdir();
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
//        headers.add("Content-Disposition", "attachment; filename="+imageId+".jpg");
        headers.setContentType(MediaType.IMAGE_JPEG);
        File apkfile = new File(Pa.CUSTOMER_ORDER_PATH+"temp"+File.separator+imageId+".jpg");
        FileInputStream fi=new FileInputStream(apkfile);
        byte[] bs=IOUtils.toByteArray(fi);
        fi.close();
        return new ResponseEntity<byte[]>(bs, headers, HttpStatus.CREATED);
    }
//-------------------------------------------------------------------------------------------------------------資料上傳